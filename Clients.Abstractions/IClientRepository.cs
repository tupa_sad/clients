﻿using Clients.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Clients.Abstractions
{
    public interface IClientRepository
    {
        Task<IEnumerable<Client>> GetAllClients(int page, int perPage);
        Task<Client> GetClient(string сlientId);
        Task<Client> CreateClientAsync(Client сlient);
        Task DeleteClientAsync(Client сlient);
        Task UpdateClientAsync(Client client);
    }
}
