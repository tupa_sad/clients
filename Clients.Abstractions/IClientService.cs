﻿using Clients.Abstractions.Models;
using Clients.Abstractions.Requests;
using Clients.Abstractions.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Clients.Abstractions
{
    public interface IClientService
    {
        Task<IList<ClientResponse>> GetAllAsResponses(int page, int perPage);
        Task<Client> Get(string сlientId);
        Task<Client> Create(Client сlient);
        Task Delete(string сlientId);
        Task<Client> Update(UpdateClientRequest updateClientRequest);
    }
}
