﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clients.Abstractions.Models
{
    public partial class Client : HaveFirstLastName
    {
        public string ClientId { get; set; }
    }
}
