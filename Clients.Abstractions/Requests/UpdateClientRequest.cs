﻿using Clients.Abstractions.Models;
using Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clients.Abstractions.Requests
{
    public class UpdateClientRequest : HaveFirstLastName
    {
        public string ClientId { get; set; }

        public void UpdateClient(ref Client client)
        {
            client.FirstName = FirstName ?? client.FirstName;
            client.LastName = LastName ?? client.LastName;
        }
    }
}
