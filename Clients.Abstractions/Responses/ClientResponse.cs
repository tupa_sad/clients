﻿using Clients.Abstractions.Models;
using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clients.Abstractions.Responses
{
    public class ClientResponse : HaveFirstLastName
    {
        public ClientResponse(Client client)
        {
            ClientId = client.ClientId;
            FirstName = client.FirstName;
            LastName = client.LastName;
        }

        public string ClientId { get; set; }
    }
}
