﻿using Clients.Abstractions;
using Clients.Api.Context;
using Clients.Api.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clients.Api.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection InitializeClientsServices(
            this IServiceCollection services,
            Action<DbContextOptionsBuilder> options = null)
        {
            services.AddDbContext<ClientContext>(options);
            services.TryAddScoped<IClientRepository, ClientRepository>();
            services.TryAddScoped<IClientService, ClientService>();
            return services;
        }
    }
}
