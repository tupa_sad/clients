﻿using Clients.Abstractions;
using Clients.Abstractions.Models;
using Clients.Abstractions.Requests;
using Clients.Abstractions.Responses;
using Clients.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clients.Api.Services
{
    public class ClientService : IClientService
    {
        private readonly ClientRepository clientRepository;

        public ClientService(ClientRepository clientRepository)
        {
            this.clientRepository = clientRepository;
        }

        public async Task<Client> Create(Client сlient)
        {
            return await clientRepository.CreateClientAsync(сlient);
        }

        public async Task Delete(string сlientId)
        {
            Client client = await clientRepository.GetClient(сlientId);
            if (client == null)
            {
                await clientRepository.DeleteClientAsync(client);
            }
            return;
        }

        public async Task<Client> Get(string сlientId)
        {
            Client client = await clientRepository.GetClient(сlientId);
            if (client == null)
            {
                return null;
            }
            return client;
        }

        public async Task<IList<ClientResponse>> GetAllAsResponses(int page, int perPage)
        {
            var clientsDb = await clientRepository.GetAllClients(page, perPage);

            var clients = new List<ClientResponse>();
            foreach (var clientDb in clientsDb)
            {
                clients.Add(new ClientResponse(clientDb));
            }

            return clients;
        }

        public async Task<Client> Update(UpdateClientRequest updateClientRequest)
        {
            Client client = await clientRepository.GetClient(updateClientRequest.ClientId);
            updateClientRequest.UpdateClient(ref client);
            await clientRepository.UpdateClientAsync(client);
            return client;
        }
    }
}
