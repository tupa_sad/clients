﻿using Clients.Abstractions;
using Clients.Abstractions.Models;
using Clients.Api.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clients.Api.Services
{
    public class ClientRepository : IClientRepository
    {
        private readonly ClientContext context;

        public async Task<IEnumerable<Client>> GetAllClients(int page, int perPage)
        {
            IQueryable<Client> request = context.Clients;
            if (page > 0 && perPage > 0)
            {
                request = request.Skip((page - 1) * perPage).Take(perPage);
            }

            request = request.OrderBy(x => x.LastName);
            return await request.ToListAsync();
        }

        public async Task<Client> GetClient(string clientId)
        {
            Client client = context.Clients.FirstOrDefault(x => x.ClientId == clientId);
            if(client == null)
            {
                throw new Exception($"Client with '{clientId}' id is not found. ({nameof(ClientRepository)}.{nameof(GetClient)})");
            }
            return client;
        }

        public async Task<Client> CreateClientAsync(Client client)
        {
            context.Clients.Add(client);
            await context.SaveChangesAsync();
            return client;
        }

        public async Task DeleteClientAsync(Client client)
        {
            context.Remove(client);
            await context.SaveChangesAsync();
        }

        public async Task UpdateClientAsync(Client client)
        {
            context.Update(client);
            await context.SaveChangesAsync();
        }
    }
}
